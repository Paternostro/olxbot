﻿namespace CrossCrawler {
    public struct Ad {

        public string title;
        public double price;
        public string location;
        public string url;

        public Ad(string title, double price, string location, string url) {
            this.title = title;
            this.price = price;
            this.location = location;
            this.url = url;
        }
    }
}
