﻿using System;

namespace CrossCrawler {
    [Serializable]
    public class ItemCategory {

        public static ItemCategory ComputersAccessories = new ItemCategory("computadores-e-acessorios", 0);
        public static ItemCategory AudioVideoPhotoTV = new ItemCategory("audio-tv-video-e-fotografia", 1);
        public static ItemCategory Videogames = new ItemCategory("videogames", 2);
        public static ItemCategory Cellphones = new ItemCategory("celulares", 3);
        public static ItemCategory ClothesShoes = new ItemCategory("roupas-e-calcados", 4);
        public static ItemCategory Antiques = new ItemCategory("antiguidades", 5);
        public static ItemCategory CollectionsHobbies = new ItemCategory("hobbies-e-colecoes", 6);
        public static ItemCategory MusicalInstruments = new ItemCategory("instrumentos-musicais", 7);
        public static ItemCategory HomeFurniture = new ItemCategory("moveis", 8);
        public static ItemCategory HomeAppliances = new ItemCategory("eletrodomesticos", 9);
        public static ItemCategory HomeDecor = new ItemCategory("objetos-de-decoracao", 10);
        public static ItemCategory HomeUtility = new ItemCategory("utilidades-domesticas", 11);
        public static ItemCategory SportsGym = new ItemCategory("esportes-e-ginastica", 12);
        public static ItemCategory Cycling = new ItemCategory("ciclismo", 13);
        public static ItemCategory CommercialEquipmentFurniture = new ItemCategory("comercio-e-escritorio/equipamentos-e-mobiliario", 14);
        public static ItemCategory CommercialOtherStuff = new ItemCategory("comercio-e-escritorio/outros-itens-para-comercio-e-escritorio", 15);
        public static ItemCategory ComercialVehicles = new ItemCategory("comercio-e-escritorio/trailers-e-carrinhos-comerciais", 16);

        public string linkSuffix;
        public int index;

        public ItemCategory(string linkSuffix, int index) {
            this.linkSuffix = linkSuffix;
            this.index = index;
        }

        public ItemCategory() { }
    }
}
