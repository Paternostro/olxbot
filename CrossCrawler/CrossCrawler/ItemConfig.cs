﻿using System;

namespace CrossCrawler {
    [Serializable]
    public struct ItemConfig {

        public Terms terms;

        public int minPrice, maxPrice;

        public ItemCategory category;

        public ItemConfig(ItemCategory category, string[] searchTerms, string[] forbiddenTerms, string[] adequateTerms, string[] inadequateTerms, int minPrice, int maxPrice) {
            this.category = category;
            this.terms.search = searchTerms;
            this.terms.forbidden = forbiddenTerms;
            this.terms.adequate = adequateTerms;
            this.terms.inadequate = inadequateTerms;
            this.minPrice = minPrice;
            this.maxPrice = maxPrice;
        }

        public struct Terms {
            public string[] search;
            public string[] forbidden;
            public string[] adequate;
            public string[] inadequate;
        }
    }
}
