﻿using System.Collections.Generic;

namespace CrossCrawler {
    public class Item {
        public ItemConfig config;
        public Dictionary<string, AdData> adDataDict;

        public Item(ItemConfig itemConfig, int historySize) {
            this.config = itemConfig;
            adDataDict = new Dictionary<string, AdData>();

            foreach (string sTerm in itemConfig.terms.search) {
                adDataDict.Add(sTerm, new AdData(historySize));
            }
        }
    }
}
