﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Timers;
using System.Xml.Serialization;
using HtmlAgilityPack;

namespace CrossCrawler {
    class Program {

        static Timer searchTimer;
        static HtmlWeb web = new HtmlWeb();
        static List<Item> items = new List<Item>();
        static CultureInfo culturaBrasileira = new CultureInfo("pt-BR");

        static int historySize = 50;
        static string[] desirableCities = new string[] {
            "Vitória - ES",
            "Vila Velha - ES",
            "Serra - ES"
        };

        static void Main(string[] args) {

            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            web.OverrideEncoding = Encoding.GetEncoding("ISO-8859-1");
            XmlSerializer reader = new XmlSerializer(typeof(ItemConfig));

            Console.WriteLine("OLXCrawler has started.");
            Console.WriteLine("BaseDir is: " + baseDir);
            Console.WriteLine("The following files were loaded:");

            foreach (string file in Directory.EnumerateFiles(baseDir, "*.xml")) {
                FileStream fs = File.OpenRead(file);
                items.Add(new Item((ItemConfig)reader.Deserialize(fs), historySize));
                string fileName = file.Substring(file.LastIndexOf('\\') + 1);
                Console.WriteLine(fileName);
            }
            try {
                if (args.Length != 0) {
                    SetTimer(int.Parse(args[0]));
                }
                else {
                    SetTimer(300000);
                }
            }
            catch {
                Console.WriteLine("Couldn't read first arg (interval). Program will shutdown.");
            }

            Console.WriteLine("Crawler is running, press Return to terminate.");

            Console.ReadLine();
        }

        static void SetTimer(int interval) {
            // Create a timer with a interval.
            searchTimer = new System.Timers.Timer(interval);
            // Hook up the Elapsed event for the timer.
            searchTimer.Elapsed += Search;
            searchTimer.AutoReset = true;
            searchTimer.Enabled = true;

        }

        static void Search(Object source, ElapsedEventArgs e) {

            string linkPrefix = "https://www.olx.com.br/";
            string minPricePrefix = "?pe=";
            string maxPricePrefix = "&ps=";
            string searchTermPrefix = "&q=";

            Console.WriteLine("The Search event was raised at {0:HH:mm:ss.fff}", e.SignalTime);

            string mailBody = "OLX Bot has encontered the following trade opportunities:\n\n";

            bool mustSendEmail = false;

            foreach (Item item in items) {

                mailBody += item.config.terms.search[0] + " offers:\n";

                List<string> removedTitles = new List<string>();
                List<string> repeatedTitles = new List<string>();

                int a = 0;

                foreach (string searchTerm in item.config.terms.search) {

                    AdData termAdData = item.adDataDict[searchTerm];

                    string treatedSearchTerm = searchTerm.Replace(' ', '+');

                    string link = linkPrefix + item.config.category.linkSuffix + minPricePrefix
                        + item.config.minPrice.ToString() + maxPricePrefix + item.config.maxPrice.ToString()
                        + searchTermPrefix + treatedSearchTerm;

                    var htmlDoc = web.Load(link);

                    HtmlNodeCollection ads = htmlDoc.DocumentNode.SelectNodes("//div[@class='section_OLXad-list ']/ul/li/a");

                    for (int i = 0; i < ads.Count; i++) {
                        // get title
                        string title = ads[i].Attributes["title"].Value;
                        //string title = items[i].SelectSingleNode("div[@class='col-2']/div[@class='OLXad-list-line-1 mb5px']/h2[@class='OLXad-list-title mb5px']").InnerText.Trim();

                        double price;
                        // TRY to get price
                        try {
                            string priceTag = ads[i].SelectSingleNode("div[@class='col-3']/p[@class='OLXad-list-price']").InnerText.Trim();
                            price = Double.Parse(priceTag, NumberStyles.Currency, culturaBrasileira);
                        }
                        catch (NullReferenceException ex) {
                            continue;
                        }

                        // get location
                        string dirtyLocation = ads[i].SelectSingleNode("div[@class='col-2']/div[@class='OLXad-list-line-2']/p[@class='text detail-region']").InnerText.Trim();
                        string location = System.Text.RegularExpressions.Regex.Replace(dirtyLocation, @"\s+", " ");

                        // get link
                        string dirtyUrl = ads[i].Attributes["href"].Value;
                        string url = dirtyUrl.Substring(0, dirtyUrl.IndexOf('?'));

                        // TRY to get image
                        //try {
                        //    string imageLink = items[i].SelectSingleNode("div[@class='col-1']/div[@class='OLXad-list-image']/div[@class='OLXad-list-image-box']/img[@class='image']").Attributes["src"].Value;
                        //    Image image = new Image();
                        //    image.Source = new BitmapImage(new Uri(imageLink));
                        //}
                        //catch (NullReferenceException ex) {
                        //    continue;
                        //}

                        // add to list
                        item.adDataDict[searchTerm].currentAds.Add(new Ad(title, price, location, url));
                    }

                    repeatedTitles.AddRange(item.adDataDict[searchTerm].RemoveCurrentInHistory());

                    for (int i = termAdData.currentAds.Count - 1; i >= 0; i--) {

                        // IF ad title has forbidden words, discard the ad.
                        bool hasForbidden = false;
                        bool isFar = true;
                        foreach (string forbidden in item.config.terms.forbidden) {
                            if (termAdData.currentAds[i].title.IndexOf(forbidden, StringComparison.CurrentCultureIgnoreCase) >= 0) {
                                removedTitles.Add(termAdData.currentAds[i].title);
                                termAdData.currentAds.RemoveAt(i);
                                hasForbidden = true;
                                break;
                            }
                        }
                        if (hasForbidden) {
                            continue;
                        }

                        // Check if ad city is near you (AKA is in desirable cities).
                        foreach (string city in desirableCities) {
                            if (termAdData.currentAds[i].location == city) {
                                isFar = false;
                                break;
                            }
                        }
                        if (isFar) {
                            removedTitles.Add(termAdData.currentAds[i].title);
                            termAdData.currentAds.RemoveAt(i);
                            continue;
                        }

                        // IF inadequate term is before adequate term on title, discard the ad.
                        foreach (string inadequate in item.config.terms.inadequate) {
                            int inadequateIndex = termAdData.currentAds[i].title.IndexOf(inadequate,
                                StringComparison.CurrentCultureIgnoreCase);
                            if (inadequateIndex >= 0) {
                                bool mustDelete = true;
                                foreach (string adequate in item.config.terms.adequate) {
                                    int adequateIndex = termAdData.currentAds[i].title.IndexOf(adequate,
                                        StringComparison.CurrentCultureIgnoreCase);
                                    if (adequateIndex < inadequateIndex && adequateIndex >= 0) {
                                        mustDelete = false;
                                    }
                                }
                                if (mustDelete) {
                                    removedTitles.Add(termAdData.currentAds[i].title);
                                    termAdData.currentAds.RemoveAt(i);
                                    break;
                                }
                            }
                        }
                    }

                    // Ad passed all tests, put the good boy in the email (and history).
                    foreach (Ad ad in termAdData.currentAds) {
                        mailBody += a++ + ": " + ad.title + " | " + ad.price + " | " + ad.location + "\n" + ad.url + "\n";
                        Console.WriteLine(a++ + ": " + ad.title + " | " + ad.price + " | " + ad.location + "\n" + ad.url);
                    }
                    if (termAdData.currentAds.Count > 0) {
                        mustSendEmail = true;
                        termAdData.UpdateHistory();
                    }
                }

                //foreach (string title in removedTitles) {
                //    Console.WriteLine("Removed: " + title);
                //}

                //foreach (string title in repeatedTitles) {
                //    Console.WriteLine("Repeated: " + title);
                //}
            }
            if (mustSendEmail) {
                try {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress("aureus.resale@gmail.com");
                    mail.To.Add("pedro.paternostro@gmail.com");
                    mail.Subject = "OLX Resale Opportunity";
                    mail.Body = mailBody;

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("aureus.resale@gmail.com", "jQk7I$Yu1JyRlNSjWC");
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                }
            }
            else {
                Console.WriteLine("Nothing was found.");
            }

            //        int pageNumber = 2;
            //        string pagePrefix = "o=";
            //        string pageSuffix = "&";
        }
    }
}
