﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace CrossCrawler {
    public class TestClass {

        public static void doSearch(string[] searchNames) {

            foreach (string name in searchNames) {

                CultureInfo culturaBrasileira = new CultureInfo("pt-BR");

                HtmlWeb web = new HtmlWeb();

                var htmlDoc = web.Load("https://www.olx.com.br/videogames?pe=1000&ps=500&q=" + name);

                HtmlNodeCollection items = htmlDoc.DocumentNode.SelectNodes("//div[@class='section_OLXad-list ']/ul/li/a");

                List<Ad> adsData = new List<Ad>();

                for (int i = 0; i < items.Count; i++) {
                    // get title
                    string title = items[i].SelectSingleNode("div[@class='col-2']/div[@class='OLXad-list-line-1 mb5px']/h2[@class='OLXad-list-title mb5px']").InnerText.Trim();

                    double price;
                    // TRY to get price
                    try {
                        string priceTag = items[i].SelectSingleNode("div[@class='col-3']/p[@class='OLXad-list-price']").InnerText.Trim();
                        price = Double.Parse(priceTag, NumberStyles.Currency, culturaBrasileira);
                    }
                    catch (NullReferenceException ex) {
                        continue;
                    }

                    // get location
                    string dirtyLocation = items[i].SelectSingleNode("div[@class='col-2']/div[@class='OLXad-list-line-2']/p[@class='text detail-region']").InnerText.Trim();
                    string location = System.Text.RegularExpressions.Regex.Replace(dirtyLocation, @"\s+", " ");

                    // add to list
                    adsData.Add(new Ad(title, price, location, ""));

                }

                int a = 0;
                foreach (Ad ad in adsData) {
                    a++;
                    Console.Write(a + ": " + ad.title + " | " + ad.price + " | " + ad.location + "\n");
                }
            }
        }

        public void writeTestHtml(string url) {

            HtmlWeb web = new HtmlWeb();

            var htmlDoc = web.Load(url);

            StreamWriter streamWriter = new StreamWriter("test.html");

            htmlDoc.Save(streamWriter);
        }

    }
}
