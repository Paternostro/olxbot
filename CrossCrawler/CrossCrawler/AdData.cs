﻿using System;
using System.Collections.Generic;

namespace CrossCrawler {
    public class AdData {
        public List<Ad> currentAds;
        public string[] urlHistory;

        public AdData(int historySize) {
            this.urlHistory = new string[historySize];
            currentAds = new List<Ad>();
        }

        public string[] RemoveCurrentInHistory() {
            List<string> repeated = new List<string>();
            for (int i = currentAds.Count - 1; i >= 0; i--) {
                foreach (string url in urlHistory) {
                    if (currentAds[i].url == url) {
                        repeated.Add(currentAds[i].title);
                        currentAds.RemoveAt(i);
                        break;
                    }
                }
            }
            return repeated.ToArray();
        }

        public void UpdateHistory() {
            //string[] noOldies = urlHistory.Take(urlHistory.Length - currentAds.Count).ToArray();
            Array.ConstrainedCopy(urlHistory, 0, urlHistory, currentAds.Count, urlHistory.Length - currentAds.Count);
            for (int i = 0; i < currentAds.Count; i++) {
                urlHistory[i] = currentAds[i].url;
            }
        }
    }
}
